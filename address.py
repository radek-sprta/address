#! /usr/bin/env python3

# Script to open address from clipboard or command-line in OpenStreetMap.org.
#
# Usage: address.py | address.py ADDRESS
#
# Author: Radek Sprta <mail@radeksprta.eu>
# Date: 2017-03-04
# License: The MIT License
import sys
import webbrowser

import pyperclip


def get_address():
    """
    Gets address from clipboard or commandline.
    """
    if len(sys.argv) > 1:
        # Get address from command line
        address = ''.join(sys.argv[1:])
    else:
        # Get address from clipboard
        address = pyperclip.paste()
    return address


def main():
    """
    Open address in OpenStreetMap.org using default web browser.
    """
    address = get_address()
    webbrowser.open(
        'https://www.openstreetmap.org/search?query={}'.format(address))

if __name__ == '__main__':
    main()
